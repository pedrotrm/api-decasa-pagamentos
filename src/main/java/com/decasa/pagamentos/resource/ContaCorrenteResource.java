package com.decasa.pagamentos.resource;


import com.decasa.pagamentos.model.ContaCorrente;
import com.decasa.pagamentos.service.ContaCorrenteService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/conta")
public class ContaCorrenteResource {


    private final ContaCorrenteService service;

    public ContaCorrenteResource(ContaCorrenteService service) {
        this.service = service;
    }

    @GetMapping(value = "/prestador/{prestadorId}/contaCorrente")
    public ResponseEntity<List<ContaCorrente>> findContaCorrenteOfPrestador(@PathVariable Long prestadorId){
        List<ContaCorrente> result = service.consultarExtrato(prestadorId);
        return ResponseEntity.ok().body(result);
    }
}
