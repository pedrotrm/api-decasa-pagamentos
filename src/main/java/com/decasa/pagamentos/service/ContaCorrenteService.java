package com.decasa.pagamentos.service;


import com.decasa.pagamentos.model.ContaCorrente;
import com.decasa.pagamentos.repository.ContaCorrenteRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

@Service
public class ContaCorrenteService {

    private final ContaCorrenteRepository repository;

    public ContaCorrenteService(ContaCorrenteRepository repository) {
        this.repository = repository;
    }


    public List<ContaCorrente> consultarExtrato(Long prestadorId){

        List<ContaCorrente> result = repository.findContaCorrenteByPrestadorId(prestadorId);

        for (ContaCorrente conta : result){
           Integer codigoOrcamento = conta.getFatorGerado().intValue();
           conta.setCodigoOrcamento("BR"+codigoOrcamento.toString()+"MA");
        }
        return result;
    }
}
