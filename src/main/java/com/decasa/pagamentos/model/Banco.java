package com.decasa.pagamentos.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "qg62_banco")
public class Banco implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "qg62_nome")
    private String nome;

    @Column(name= "qg62_sigla")
    private String sigla;

    @Column(name = "qg62_qg62_cod_febraban")
    private String codigoFenabran;

    public Banco() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getSigla() {
        return sigla;
    }

    public void setSigla(String sigla) {
        this.sigla = sigla;
    }

    public String getCodigoFenabran() {
        return codigoFenabran;
    }

    public void setCodigoFenabran(String codigoFenabran) {
        this.codigoFenabran = codigoFenabran;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Banco banco = (Banco) o;

        return id != null ? id.equals(banco.id) : banco.id == null;
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }
}
