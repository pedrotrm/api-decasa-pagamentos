package com.decasa.pagamentos.model;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name = "qg60_conta_corrente")
public class ContaCorrente implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "qg60_cod_conta_corrente")
    private  Long id;

    @Column(name = "qg60_agencia")
    private String agencia;

    @Column(name = "qg60_conta")
    private String codigoConta;

    @Column(name = "qg60_digito_agencia")
    private String digitoAgencia;

    @Column(name = "qg60_digito_conta")
    private String digitoConta;

    @Column(name = "qg60_cod_pessoa")
    private Long codigoPessoa;

    @Column(name = "qg60_fator_gerador")
    private Double fatorGerado;

    @Column(name = "qg60_data_lancamento")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss", timezone = "GMT")
    private Date dataLancamento;

    @Column(name = "qg60_data_transferencia")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss", timezone = "GMT")
    private Date dataTransferencia;

    @Column(name = "qg60_operacao")
    private String operacao;

    @Column(name = "qg60_valor")
    private BigDecimal valor;

    @Column(name = "fkqg60qg49_cod_usuario")
    private Long codigoUsuario;

    @OneToOne
    @JoinColumn(name = "fkqg60qg62_cod_banco")
    private Banco banco;

    @OneToOne
    @JoinColumn(name = "fkqg60qg58_cod_conta")
    private Conta conta;

    @OneToOne
    @JoinColumn(name = "fkqg60qg59_cod_cc_tipo_pessoa")
    private TipoPessoa tipoPessoa;

    @Transient
    private String codigoOrcamento;


    public ContaCorrente() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAgencia() {
        return agencia;
    }

    public void setAgencia(String agencia) {
        this.agencia = agencia;
    }

    public String getCodigoConta() {
        return codigoConta;
    }

    public void setCodigoConta(String codigoConta) {
        this.codigoConta = codigoConta;
    }

    public String getDigitoAgencia() {
        return digitoAgencia;
    }

    public void setDigitoAgencia(String digitoAgencia) {
        this.digitoAgencia = digitoAgencia;
    }

    public String getDigitoConta() {
        return digitoConta;
    }

    public void setDigitoConta(String digitoConta) {
        this.digitoConta = digitoConta;
    }

    public Long getCodigoPessoa() {
        return codigoPessoa;
    }

    public void setCodigoPessoa(Long codigoPessoa) {
        this.codigoPessoa = codigoPessoa;
    }

    public Double getFatorGerado() {
        return fatorGerado;
    }

    public void setFatorGerado(Double fatorGerado) {
        this.fatorGerado = fatorGerado;
    }

    public Date getDataLancamento() {
        return dataLancamento;
    }

    public void setDataLancamento(Date dataLancamento) {
        this.dataLancamento = dataLancamento;
    }

    public Date getDataTransferencia() {
        return dataTransferencia;
    }

    public void setDataTransferencia(Date dataTransferencia) {
        this.dataTransferencia = dataTransferencia;
    }

    public String getOperacao() {
        return operacao;
    }

    public void setOperacao(String operacao) {
        this.operacao = operacao;
    }

    public BigDecimal getValor() {
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }

    public Long getCodigoUsuario() {
        return codigoUsuario;
    }

    public void setCodigoUsuario(Long codigoUsuario) {
        this.codigoUsuario = codigoUsuario;
    }

    public Banco getBanco() {
        return banco;
    }

    public void setBanco(Banco banco) {
        this.banco = banco;
    }

    public Conta getConta() {
        return conta;
    }

    public void setConta(Conta conta) {
        this.conta = conta;
    }

    public TipoPessoa getTipoPessoa() {
        return tipoPessoa;
    }

    public void setTipoPessoa(TipoPessoa tipoPessoa) {
        this.tipoPessoa = tipoPessoa;
    }

    public String getCodigoOrcamento() {
        return codigoOrcamento;
    }

    public void setCodigoOrcamento(String codigoOrcamento) {
        this.codigoOrcamento = codigoOrcamento;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ContaCorrente that = (ContaCorrente) o;

        return id != null ? id.equals(that.id) : that.id == null;
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }
}
