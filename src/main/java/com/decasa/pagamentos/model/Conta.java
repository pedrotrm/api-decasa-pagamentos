package com.decasa.pagamentos.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "qg58_conta")
public class Conta implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "qg58_cod_conta")
    private Long id;

    @Column(name = "qg58_descricao")
    private String descricao;

    @Column(name = "qg58_servico")
    private Boolean servico;

    @Column(name = "qg58_material")
    private Boolean material;

    @Column(name = "qg58_taxa_multa")
    private Boolean taxaMulta;

    @Column(name = "qg58_manual")
    private Boolean manual;

    @OneToOne
    @JoinColumn(name = "fkqg58qg57_cod_tipo_conta")
    private TipoConta tipoConta;


    public Conta() {
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Boolean getServico() {
        return servico;
    }

    public void setServico(Boolean servico) {
        this.servico = servico;
    }

    public Boolean getMaterial() {
        return material;
    }

    public void setMaterial(Boolean material) {
        this.material = material;
    }

    public Boolean getTaxaMulta() {
        return taxaMulta;
    }

    public void setTaxaMulta(Boolean taxa_multa) {
        this.taxaMulta = taxa_multa;
    }

    public Boolean getManual() {
        return manual;
    }

    public void setManual(Boolean manual) {
        this.manual = manual;
    }

    public TipoConta getTipoConta() {
        return tipoConta;
    }

    public void setTipoConta(TipoConta tipoConta) {
        this.tipoConta = tipoConta;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Conta conta = (Conta) o;

        return id != null ? id.equals(conta.id) : conta.id == null;
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }
}
