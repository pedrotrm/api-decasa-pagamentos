package com.decasa.pagamentos.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "qg57_tipo_conta")
public class TipoConta implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "qg57_cod_tipo_conta")
    private Long id;

    @Column(name = "qg57_descricao")
    private String descricao;

    public TipoConta() {
    }

    public TipoConta(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TipoConta tipoConta = (TipoConta) o;

        return id != null ? id.equals(tipoConta.id) : tipoConta.id == null;
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

}
