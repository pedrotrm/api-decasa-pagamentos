package com.decasa.pagamentos.repository;


import com.decasa.pagamentos.model.ContaCorrente;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ContaCorrenteRepository extends JpaRepository<ContaCorrente, Long> {

    @Query("select c from ContaCorrente c where c.codigoPessoa = ?1 and c.conta.id = 1 and c.tipoPessoa.id = 2")
    List<ContaCorrente> findContaCorrenteByPrestadorId(Long prestadorId);

}
